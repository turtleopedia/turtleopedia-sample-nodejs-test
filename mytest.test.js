const request = require('supertest');
const app = require('./app');
const helper = require('./helper');

let server;

beforeEach(() => {
    server = request(app);
});

describe('custom test cases turtleopedia', () => {
    it('GET request', async () => {
        const url = '/';
        const response = await server.get(url);
        expect(response.status).toEqual(200);
    });

    it('POST request', async () => {
        const url = '/';
        const body = {number: 1}
        const response = await server.post(url).send(body);
        expect(response.status).toEqual(200);
        expect(response.body.number).toEqual(2);
    });

    it('GET request without mock', async () => {
        const url = '/mock';
        const response = await server.get(url);
        expect(response.status).toEqual(200);
        expect(response.body.text).toEqual('Welcome to turtleopedia');
    });

    it('GET request without mock', async () => {
        helper.helperFunction = jest.fn(() => {
            return 'Mock Successful';
        })

        const url = '/mock';
        const response = await server.get(url);
        expect(response.status).toEqual(200);
        expect(response.body.text).toEqual('Mock Successful');
    });
});
