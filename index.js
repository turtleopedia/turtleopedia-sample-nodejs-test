const port = 3000;
const app = require('./app');

app.listen(port, () => {
    console.log(`Turtleopedia app listening at http://localhost:${port}`)
})
