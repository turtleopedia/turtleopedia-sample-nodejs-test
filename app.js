const express = require('express')
const app = express()
app.use(express.json());

const helper = require('./helper');

app.get('/', (req, res) => {
    return res.status(200).json('Hello World!')
})

app.post('/', (req, res) => {
    const number = req.body.number + 1;
    return res.status(200).json({number: number})
})

app.get('/mock', (req, res) => {
    const helperText = helper.helperFunction();
    return res.status(200).json({text: helperText});
})

module.exports = app;